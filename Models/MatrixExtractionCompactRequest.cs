using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatrixSynthesisWebApp.Models
{
    public class MatrixExtractionCompactRequest
    {
        public double[] freq {get; set;}
        public double[] S11_db {get; set;}
        public double[] S11_angRad {get; set;}
        public double[] S21_db {get; set;}
        public double[] S21_angRad {get; set;}

        // filter info
        public double[,] tranZeros {get; set;}
        public int filterOrder {get; set;}
        public double centerFreq {get; set;}
        public double bandwidth {get; set;}

        // tuning parameters
        public double captureStartFreqGHz {get; set;}
        public double captureStopFreqGHz {get; set;} 
        public bool isSymmetric {get; set;}

        // topology structure
        public int[,] topology {get; set;}

        // golden matrix
        public double[,] targetMatrix {get; set;}
    }
}
