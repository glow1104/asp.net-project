using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatrixSynthesisWebApp.Models
{
    public class MatrixExtractionRequest
    {
        // data file Info
        public string dataFileName {get; set;}

        // Matrix Info
        public int tn {get; set;}
        public double qu {get; set;}
        public double r1 {get; set;} // input
        public double rn {get; set;} // output
        public double fL {get; set;} // display from
        public double fU {get; set;} // display to

        // Filter Info
        public int N {get; set;} // FilterOrder
        public int Nz {get; set;} // tx Zero Number
        public int[] Z {get; set;} // Zero Frequency
        public double f0 {get; set;}
        public double bw {get; set;}
        public double[] prm {get; set;}
        public double[] ftz {get; set;}

        // Golden Matrix
        public double[,] goldenMatrix {get; set;}

        // Custom Topology
        public int[,] customStructure {get; set;}
        public int[] CT {get; set;}
        
        // Tuning Info
        public double FL {get; set;}
        public double FH {get; set;}
        public int lossy {get; set;}
        public int structType {get; set;}
    }
}
