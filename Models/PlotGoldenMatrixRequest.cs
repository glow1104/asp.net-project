using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatrixSynthesisWebApp.Models
{
    public class PlotGoldenMatrixRequest
    {
        // Matrix Info
        public int tn {get; set;}
        public double qu {get; set;}
        public double r1 {get; set;} // input
        public double rn {get; set;} // output
        public double fL {get; set;} // display from
        public double fU {get; set;} // display to

        // Filter Info
        public double f0 {get; set;}
        public double bw {get; set;}

        // Golden Matrix
        public double[,] goldenMatrix {get; set;}
    }
}
