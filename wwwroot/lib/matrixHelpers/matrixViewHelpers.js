define(function(){
    return {
        loadData: function(matrix, $mainContainer, options){
            var isDeltaMatrix = false;
            var errorValue = 0.001;
            if(options && options.isDeltaMatrix){
                isDeltaMatrix = options.isDeltaMatrix;

                if(options.errorValue){
                    errorValue = options.errorValue;
                }
            }
            var order = matrix.length;

            // create container
            var $matrix = $('<div class="golden-matrix__container"></div>');            
            
            // add classes if it is a delta matrix
            if(isDeltaMatrix){
                $matrix.addClass('delta-matrix')
            }

            // create table container            
            var $table = $('<div class="matrix__table"></div>');                    
            
            // create data items
            // for each item in the golden matrix (rows), there is another array (columns)
            _.each(matrix, function(matrixRow, rowIndex){
                // if first item, then create top row for order
                if(rowIndex === 0){
                    var $headerRow = $('<div class="matrix__row--header"></div>');
                    for(var j = 0; j < (order + 1); j++){
                        var orderStr = j - 1;
                        if(j === 0){
                            orderStr = '';
                        }else if(j === 1){
                            orderStr = 'S';
                        }else if(j === order){
                            orderStr = 'L';
                        }
                        
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $headerRow.append($headerItem);
                    }
                    $table.append($headerRow);
                }
                
                // create row
                var $row = $('<div class="matrix__row"></div>');
                // go through each value
                _.each(matrixRow, function(matrixItem, colIndex){
                    // add row order at begining 
                    if(colIndex === 0){
                        var orderStr = rowIndex;
                        if(rowIndex === 0){
                            orderStr = 'S';
                        }else if(rowIndex === (order - 1)){
                            orderStr = 'L';
                        }
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $row.append($headerItem);
                    }

                    // determine level index
                    var levelIndex = Math.abs(rowIndex - colIndex)
                    if(levelIndex > 0){
                        if(matrixItem == 0){
                            levelIndex = '';
                        } else if(levelIndex > 2){
                            levelIndex = 'x';
                        }
                    }
                    
                    // create checkbox container
                    var $valueContainer = $('<div class="matrix__item level-index--' + levelIndex + '"></div>');

                    // if is delta matrix and has error, add 'error' class
                    if(isDeltaMatrix && matrixItem != 0){
                        var classToAdd = 'valid';
                        if(Math.abs(matrixItem) > errorValue){
                            classToAdd = 'error';
                        }
                        $valueContainer.addClass(classToAdd);
                    }

                    // create 
                    var $value = $('<span class="matrix__value"></span>');
                    $value.text((matrixItem).toFixed(5));
                    
                    // append the checkbox to the checkbox container
                    $valueContainer.append($value);
                    
                    // append it to the row
                    $row.append($valueContainer);
                    
                    // add row order at end 
                    if(colIndex === (order - 1)){
                        var orderStr = rowIndex;
                        if(rowIndex === 0){
                            orderStr = 'S';
                        }else if(rowIndex === (order - 1)){
                            orderStr = 'L';
                        }
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');

                        $row.append($headerItem);
                    }
                    
                });
                
                // append row to container
                $table.append($row);
                
                // if last item, then create bottom row for order
                if(rowIndex === (order - 1)){
                    var $headerRow = $('<div class="matrix__row--header"></div>');
                    for(var j = 0; j < (order + 1); j++){
                        var orderStr = j - 1;
                        if(j === 0){
                            orderStr = '';
                        }else if(j === 1){
                            orderStr = 'S';
                        }else if(j === order){
                            orderStr = 'L';
                        }
                        
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $headerRow.append($headerItem);
                    }
                    $table.append($headerRow);
                }                                
                
            });
            
            // add table
            $matrix.append($table);
            
            // add to dom element
            $mainContainer.html($matrix);
            
            // get width of 1 item and make sure the container can hold them all
            var width = $('.matrix__item').outerWidth();
            
            // total width is width of 1 item * (order + 1) where 1 is order #        
            $matrix.css({width: (width * (order + 2))});

        },

        clearMatrix: function($container){
            $container.html('');
        }
    };
});