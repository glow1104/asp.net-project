define(['d3'], function(d3){
    "use strict";
    
    var topologyGraphViewer = function() {
        this.vis = d3.select("#matrix-topology-container")
                .append("svg:svg")
                    .attr("class", "stage")
                    .attr("width", document.getElementById("matrix-topology-container").offsetWidth)
                    .attr("height", document.getElementById("matrix-topology-container").offsetHeight);

        this.vis
            .append("g")
            .attr("id", "group-links");
        this.vis
            .append("g")
            .attr("id", "group-nodes");

        d3.select("#matrix-topology-container").select("svg")
            .attr("width", document.getElementById("matrix-topology-container").offsetWidth)
            .attr("height", document.getElementById("matrix-topology-container").offsetHeight);
    };

    topologyGraphViewer.prototype.onMatrixChanged = function(M){
        this._handleChangeM(M);
    };

    topologyGraphViewer.prototype._M2Nodeslinks = function(M, unitStep){
        var N = M.length - 2, nodes = [], links = [], i, j, k, edgeIndex = 0;
        for (i = 0; i < N + 2; i++){
            var label;
            switch (i) {
                case 0:
                    label = 'S';
                    break;
                case (N + 1):
                    label = 'L';
                    break;
                default:
                    label = i.toString();
            }
            nodes.push({id: i, label: label, x: (i + 0.5) * unitStep, y: unitStep, size: 3});
        }
        for (i = 0; i < N + 1; i++){
            if (M[i][i + 1] === 1){
                links.push({id: edgeIndex, source: nodes[i], target: nodes[i + 1], size: 3, primary: true});
                edgeIndex++;
            }
        }
        for (i = 0; i < N; i++){
            for (j = N + 1; j > i + 1; j--){
                if (M[i][j] === 1){
                    links.push({id: edgeIndex, source: nodes[i], target: nodes[j], size: 2, primary: false});
                    edgeIndex++;
                    if ((j === i + 2) && (nodes[i].y === nodes[j].y)){
                        for (k = j; k < N + 2; k++){
                            nodes[k].x -= unitStep;
                        }
                        nodes[j - 1].x -= 0.5 * unitStep;
                        nodes[j - 1].y += unitStep;
                    } else if (nodes[i].y === nodes[j].y){
                        for (k = j; k < N + 2; k++){
                            nodes[k].x -= 2 * unitStep;
                        }
                        for (k = i + 1; k < j; k++){
                            nodes[k].x -= unitStep;
                            nodes[k].y += unitStep;
                        }
                    }
                }
            }
        }
        
        return {nodes: nodes, links: links}
    };

    topologyGraphViewer.prototype._handleChangeM = function(M){
        var dataM = this._M2Nodeslinks(M, 100),
        nodes = dataM.nodes,
        links = dataM.links,
        minX = _.min(nodes.map(function(o){return o.x})),
        maxX = _.max(nodes.map(function(o){return o.x})),
        minY = _.min(nodes.map(function(o){return o.y})),
        maxY = _.max(nodes.map(function(o){return o.y}));

        this.vis
            .transition().duration(750)
            .attr("viewBox", (minX - 50) + " " + (minY - 50) + " " + (maxX - minX + 100) + " " + (maxY - minY + 100))
            .attr("preserveAspectRatio", "xMidYMid meet");
        var selectLinks = this.vis.select("#group-links").selectAll("line").data(links),
            selectNodes = this.vis.select("#group-nodes").selectAll("g.node").data(nodes);

        selectLinks
            .transition().duration(750)
            .attr("x1", function(d) { return d.source.x })
            .attr("y1", function(d) { return d.source.y })
            .attr("x2", function(d) { return d.target.x })
            .attr("y2", function(d) { return d.target.y })
            .style("stroke", "#666666")
            .style("stroke-width", "10px")
            .style("stroke-dasharray", function(d){return (d.primary)? "" : "10, 4"});

        selectLinks
            .enter()
            .append("line")
            .attr("x1", function(d) { return d.source.x })
            .attr("y1", function(d) { return d.source.y })
            .attr("x2", function(d) { return d.target.x })
            .attr("y2", function(d) { return d.target.y })
            .style("stroke", "#666666")
            .style("stroke-width", "10px")
            .style("stroke-dasharray", function(d){return (d.primary)? "" : "10, 4"});
            
        selectLinks.exit().remove();
        
        selectNodes
            .transition().duration(750)
            .attr("transform", function(d){return "translate(" + d.x + "," + d.y + ")"});
        
        let eachNode = selectNodes
            .enter()
            .append("svg:g")
            .attr("class", "node")
            .attr("transform", function(d){return "translate(" + d.x + "," + d.y + ")"});
                
        eachNode
            .append("svg:circle")
            .attr("class", "node-circle")
            .attr("r", "18px")
            .attr("fill", "#ff0000");
                
        eachNode
            .append("text")
            .attr("class", "node-text")
            .attr("dy", "5px")
            .attr("text-anchor", "middle")
            .text(function(d) {
                return d.label;
            });
            
        selectNodes
            .select(".node-text")
            .text(function(d) {
                return d.label;
            });

        selectNodes.exit().remove();
    }

    /*
    ReactDOM.render(
        <ReactRedux.Provider store={store}>
            <MatrixTopologyContainer />
        </ReactRedux.Provider>,
        document.getElementById('matrix-topology-table')
    );
    */
    return topologyGraphViewer;
});

