requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js',
    
    urlArgs: 'v=0.2.2',
    
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        d3: '../lib/d3/d3',
        mathJs: '../lib/mathjs/math',
        matrix: './matrix',
        matrixHelpers: '../lib/matrixHelpers/matrixHelpers',
        matrixViewHelpers: '../lib/matrixHelpers/matrixViewHelpers',
        numeric: '../lib/numericjs/numeric',
        sortable: '../lib/rubaxa-sortable/Sortable',
        topologyGraphViewer: '../lib/topologyGraphViewer/topologyGraphViewer',
        underscore: '../lib/underscore/underscore'        
    }
});

// Start the main app logic.
requirejs([
        'mathJs',
        'matrix/projectController',
        'matrix/fileImport',
        'matrix/frequencyScale',
        'matrix/extractionAction',
        'matrix/matrixInformation',
        'matrix/filterInformation',
        'matrix/tuningParameters',
        'matrix/splot',
        'matrix/topology',
        'matrix/goldenMatrix',
        'matrix/extractedMatrix',
        'matrix/extractionErrors',
        'matrix/specificationInput',
        'matrixHelpers'],
    function(
        mathJs,
        ProjectController,
        FileImport,
        FrequencyScale,
        ExtractionAction,
        MatrixInformation,
        FilterInformation,
        TuningParameters,
        SPlot,
        Topology,
        GoldenMatrix,
        ExtractedMatrix,
        ExtractionErrors,
        SpecificationInput,
        matrixHelpers
    ) {
    $(document).ready(function () {
        // app is ready, 

        // make sure tab-pane size have a set width: the parent's width        
        var parentWidth = $('.tab-pane').parent().width();
        $('.tab-pane').width(parentWidth);
        
        // so initiliaze the project save/get
        ProjectController.initialize();

        //... and initialize file import
        FileImport.initialize();

        //... and the frequence scale
        FrequencyScale.initialize();

        //... and the extractionAction buttons
        ExtractionAction.initialize();
        
        //... and matrix information
        MatrixInformation.initialize();
        
        //... and filter information
        FilterInformation.initialize();
        
        //... and tuning parameters
        TuningParameters.initialize();
        
        //... and SPlot
        SPlot.initialize(); 

        //... and Topology
        topology = new Topology({
            $typeInputElement: $("input[name='structure-type']"),
            $orderInputElement: $('#filter-order'), 
            $containerElement: $('#topology')
        });
        topology.initialize();
        
        //... and Golden Matrix
        GoldenMatrix.initialize();

        //... and Extracted Matrix
        ExtractedMatrix.initialize();

        //... and Extraction Errors
        ExtractionErrors.initialize();

        //... and SpecificationInput
        SpecificationInput.initialize($('#specification-input .form'));

        // trigger resize
        $(window).trigger('resize');
    });

    $('.setting-item-menu').on('click', function(e){
        var $settingItemsContainer = $('.setting-items-container');
        var $ele = $(e.currentTarget);
        var clickedId = $ele.data('id');

        var $settingElement = $('#'+clickedId); 
        // if it's showing, hide it'
        var isVisible = $settingElement.is(':visible');

        $('.setting-item-menu').removeClass('active');

        if(isVisible){
            $settingElement.hide();
            $settingItemsContainer.removeClass('active');
        }else{
            // hide all other settings
            $('.setting-item-content').hide();
            $settingItemsContainer.addClass('active');
            $('#forms').show();
            $ele.addClass('active');
            $settingElement.show();
        }

        //check state of settings container
        _updateSettingsContainer();

        // trigger a resize event
        $(window).trigger('resize');
    });

    _updateSettingsContainer = function(){
        var isVisible = false;
        $('.setting-item-content').each(function(index, element){
            if($(element).is(':visible')){
                isVisible = true;
                return;
            }
        });
        if(isVisible){
            $('#forms').show();
        }else{
            $('#forms').hide();
        }
    };

    // tab changes
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });

    $("input[name='chart-tooltip']").change(_.debounce(function(){
            // on change of the enable/disable chart tooltip,
            var enable = $("input[name='chart-tooltip']:checked").val() == "on";
            $(window).trigger('sPlot:enableTooltip', enable);
        }, 100)
    );

    $("input#error-threshold__value").change(_.debounce(function(){
            // on change of the enable/disable chart tooltip,
            var threshold = $("input#error-threshold__value").val();
            $(window).trigger('extractionErrors:threshold:updated', threshold);
        }, 250)
    );    

    $(window).on('sPlot:measurement:loaded sPlot:goldenMatrix:loaded', function(e){
        $('a[href="#splot"]').tab('show');
    });

    $(window).on('goldenMatrix:loaded', function(e){
        $('a[href="#golden-matrix"]').tab('show');
    });

    $(window).on('specificationInput:data:updated', function(e, data){
        // let splot that it should update the specification inputs

        // first, convert to MHz
        var newSpecs = _.map(data.specs, function(spec){
            var cf = $(window).triggerHandler('frequencyScale:getValueForScale', {value: spec.cf, scale: 'MHz'});
            var ct = $(window).triggerHandler('frequencyScale:getValueForScale', {value: spec.ct, scale: 'MHz'});

            return {
                id: spec.id,
                db: spec.db,
                cf: cf,
                ct: ct
            };
        });

        $(window).trigger('sPlot:specificationInputs:updated', {specs: newSpecs});
    });

    $(window).on('matrixExtractionData:loaded', function(e, data){
        // this is the response from the method.
        // go through the results and raise the appropriate events
        // so that the different components can update themselves
        var tabToShow = '';

        // need to get the filter information and splot data for 
        // generating splot data
        // get the scale
        // get scale
        var scale = $(window).triggerHandler('frequencyScale:get');

        // set data options
        var options = {
            scale: scale
        };
        
        var filterInformation = $(window).triggerHandler('filterInformation:getAllOrUndefined', options);
        var sPlotMeasurementData = $(window).triggerHandler('sPlot:measurement:get');
        var matrixInformation = $(window).triggerHandler('matrixInformation:getAllOrUndefined', options);
        var goldenMatrix = $(window).triggerHandler('goldenMatrixData:get', options);
        var hasGoldenMatrix = goldenMatrix && goldenMatrix.goldenMatrix && goldenMatrix.goldenMatrix.length > 0;

        var freq = sPlotMeasurementData.freq;
        var f0 = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.f0, originalScale: 'Hz', scale: 'GHz'});
        var bw = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.bw, originalScale: 'Hz', scale: 'GHz'});

        if(data.extractedMatrix){
            var matrixData = {
                matrixData: data.extractedMatrix
            }
            $(window).trigger('extractedMatrix:loaded', matrixData);

            // get the splot data
            var freqGHz = _.map(freq, function(value){return value / 1e3;});

            splotData = matrixHelpers.CM2S(data.extractedMatrix, freqGHz, data.q, f0, bw);
            var S11 = splotData.S11.map(function(s){return 10 * Math.log(s[1].x * s[1].x + s[1].y * s[1].y) / Math.LN10});
            var S21 = splotData.S21.map(function(s){return 10 * Math.log(s[1].x * s[1].x + s[1].y * s[1].y) / Math.LN10});
            var extractedSPlotData = {
                freq: freq,
                S11: S11,
                S21: S21
            }
            $(window).trigger('sPlot:extracted:loaded', extractedSPlotData);
            tabToShow = 'splot';
        }

        // check if we have q
        if(data.q){
            var q = {
                q: data.q
            };
            $(window).trigger('matrixInformation:unloadedQ:loaded', q);
        }

        if(tabToShow.length > 0){
            $('a[href="#' + tabToShow + '"]').tab('show');
        }

        //*************************
        // Extraction Errors

        // if deviateMatrix exists and hasGoldenMatrix is true, then 
        // it's case E and we need to calculate the errors
        if(data.deviateMatrix && hasGoldenMatrix){
            var errorMatrix = {
                errorMatrix: data.deviateMatrix
            }
            $(window).trigger('extractionErrors:errorMatrix:loaded', errorMatrix);

            // get custom structure to multiply with deviate matrix
            var customTopology = $(window).triggerHandler('customTopology:get');
            var customMatrix = customTopology.customStructure;
            var filteredDeviateMatrix = mathJs.multiply(customMatrix, data.deviateMatrix);

            // freq errors
            var diag = mathJs.diag(filteredDeviateMatrix);
            var fDiag = diag.slice(1, diag.length -1);
            var F = {
                F: _.map(fDiag, function(value, index){
                    var i = 1;
                    var j = 1;
                    return {
                        x: j + index,
                        y: i + index,
                        value: value * bw
                    }
                }) 
            };
            //$(window).trigger('extractionErrors:freqErrors:loaded', F);
            
            // coupling errors
            var superDiag = mathJs.diag(filteredDeviateMatrix, 1);
            var cDiag = superDiag.slice(1, superDiag.length -1)
            var C = {
                C: _.map(cDiag, function(value, index){
                    var i = 1;
                    var j = 2;
                    return {
                        x: j + index,
                        y: i + index,
                        value: value * (-bw/2)
                    }
                })
            }
            //$(window).trigger('extractionErrors:couplingErrors:loaded', C);

            // io errors
            errorMatrixLength = filteredDeviateMatrix.length;
            var iError = filteredDeviateMatrix[0][1] * (-bw/2);
            var oError = filteredDeviateMatrix[errorMatrixLength -2][errorMatrixLength-1] * (-bw/2);
            var IO = {
                IO: [iError, oError]
            };
            //$(window).trigger('extractionErrors:ioErrors:loaded', IO);

            // zero errors
            var customSuper2Diag = mathJs.diag(customMatrix, 2);
            var super2Diag = mathJs.diag(filteredDeviateMatrix, 2); 
            // get values of filteredDeviateMatrix where custom matrix is set to 1
            nonZeroValues = _.map(customSuper2Diag, function(value, index){
                if(value === 0){
                    return null;
                }

                var i = 0;
                var j = 2;
                return {
                    x: j + index,
                    y: i + index,
                    value: super2Diag[index] * (-bw/2)
                }
            });
            var Zero = {
                Zero: _.filter(nonZeroValues, function(value){return value != null;})
            };
            //$(window).trigger('extractionErrors:zeroErrors:loaded', Zero);

            // error chart (combined errors)
            var combinedErrors = {
                F: F.F,
                C: C.C,
                IO: IO.IO,
                Zero: Zero.Zero
            }
            $(window).trigger('extractionErrors:errorChart:loaded', combinedErrors);

            // unloaded Q
            if(data.unloadedQ){ 
                var unloadedQ = {
                    unloadedQ: data.unloadedQ
                };
                $(window).trigger('extractionErrros:unloadedQ:loaded', unloadedQ);
            }    
        }
    });
});

$(window).on('resize', function(){
    // make forms column scrollable
    var windowHeight = $(window).height();
    var navBarHeight = $('.navbar').outerHeight(false); 
    var subHeaderHeight = $('.sub-header').outerHeight(true);
    var footerHeight = $('.footer').outerHeight(true); 

    var formHeight = windowHeight - navBarHeight - subHeaderHeight - footerHeight;

    $('.forms').css({
        'height': formHeight + 'px',
        'overflow-y': 'scroll'
    });
    $('.charts').css({
        'height': formHeight + 'px',
        'overflow-y': 'scroll'
    });

    // determine chart area's width
    var windowWidth = $('body').width();
    var settingItemsWidth = $('.setting-items-container').outerWidth(true);
    var formsWidth = 0;
    if($('.forms').is(':visible')){
        formsWidth = $('.forms').outerWidth(true);
    }

    var chartWidth = windowWidth - settingItemsWidth - formsWidth;

    $('.charts').css({
        'width': chartWidth + 'px'
    });

    var parentWidth = $('.tab-pane').parent().width();
    $('.tab-pane').width(parentWidth);

    $('.bottom-bar').show();
    $('.bottom-bar').width($('.tab-pane').width());

    // determin tab content height
    var chartHeight = $('.charts').outerHeight(true);
    var tabHeight = $('.nav-tabs').outerHeight(true);
    var bottomBarHeight = $('.bottom-bar').outerHeight(true);
    var tabContentHeight = chartHeight - tabHeight - bottomBarHeight;
    $('.tab-content.parent-tabs').css({
        'height': tabContentHeight + 'px',
        'overflow-y': 'scroll'
    });

    $('.tab-content.child-tabs').css({
        'height': tabContentHeight - tabHeight - 5 + 'px', // 5 is the padding
        'overflow-y': 'scroll'
    });

    // update chart
    $(window).trigger('sPlot:chart:resize');
    $(window).trigger('extractionErrors:chart:resize');

    console.log('done resizing');
})

$(document).on('change', '.btn-file :file', function() {
    var input = $(this);
    var numFiles = input.get(0).files ? input.get(0).files.length : 1;
    var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});




