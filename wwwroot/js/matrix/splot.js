define(function(){
    return {
        initialize: function(){
            var self = this;
            self.data = null;
            self.chartOptions = {
                chart: {
                    type: 'line',
                    zoomType: 'xy'
                },
                title: {
                    text: "S-Plots"
                },
                yAxis: [{ // left y axis
                    title: {
                        text: null
                    },
                    labels: {
                        align: 'left',
                        x: 3,
                        y: 16
                    },
                    showFirstLabel: false
                }, { // right y axis
                    linkedTo: 0,
                    gridLineWidth: 0,
                    opposite: true,
                    title: {
                        text: null
                    },
                    labels: {
                        align: 'right',
                        x: -3,
                        y: 16,
                        format: '{value:.,0f}'
                    },
                    showFirstLabel: false
                }],

                credits: false,

                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    itemWidth: 185
                },

                tooltip: {
                    shared: true,
                    crosshairs: [true, true],
                    valueDecimals: 3,
                    formatter: function(){return false;}
                },

                plotOptions: {
                    line: {
                        cropThreshold: 1000
                    },
                    series: {
                        cursor: 'pointer',
                        marker: {
                            lineWidth: 1
                        }
                    }
                },
            };

            $('#splot').highcharts(self.chartOptions); 

            self._chart = $('#splot').highcharts();

            $(window).on('sPlot:chart:resize', function(){
                self._chart.reflow();
            });
            
            $(window).on('sPlot:enableTooltip', function(e, enable){
                if(enable){
                    self._chart.options.tooltip.formatter = null;
                } else {
                    self._chart.options.tooltip.formatter = function(){return false};
                }
            });

            $(window).on('sPlot:measurement:loaded', function(e, data){
                self._updateMeasurementData(data);
            })

            $(window).on('sPlot:extracted:loaded', function(e, data){
                self._updateExtractedData(data);
            });
            
            $(window).on('sPlot:extractedM:loaded', function(e, data){
                self._updateExtractedMData(data);
            });

            $(window).on('sPlot:goldenMatrix:loaded', function(e, data){
                self._updateGoldenMatrixData(data);
            });
            
            $(window).on('sPlot:measurement:get', function(e){
                return self.data;
            });

            $(window).on('sPlot:specificationInputs:updated', function(e, data){
                self._clearSpecificationInputs(data.specs);                
                _.each(data.specs, function(spec){
                    self._updateChart([spec.cf, spec.ct], [spec.db, spec.db], spec.id, {showInLegend: false, seriesName: spec.id, pointMarker: false, color: '#FF0000', enableTooltip: false});
                });

                self._loadedSpecificationInputs = data.specs;
            });
        },

        _updateMeasurementData: function(data){
            var self = this;

            // need to remove all series
            self._resetChart()

            // set current data to incoming data
            self.data = data;

            if(data.S11_db){
                self._updateChart(data.freq, data.S11_db, 'Ds11_m', {seriesName:'Measurement Data - S11', dashStyle:'Solid'});
            }

            if(data.S21_db){
                self._updateChart(data.freq, data.S21_db, 'Ds21_m', {seriesName:'Measurement Data - S21', dashStyle:'Solid'});
            }
        },

        _updateExtractedData: function(data){
            var self = this;
            
            if(data.S11){
                self._updateChart(data.freq, data.S11, 'S11_extracted', {seriesName:'Extracted - S11', dashStyle:'Dot'});
            }

            if(data.S21){
                self._updateChart(data.freq, data.S21, 'S21_extracted', {seriesName:'Extracted - S21', dashStyle:'Dot'});
            }
        },

        _updateGoldenMatrixData: function(data){
            var self = this;
            
            if(data.S11){
                self._updateChart(data.freq, data.S11, 'S11_goldenMatrix', {seriesName:'GoldenMatrix - S11', dashStyle:'ShortDash'});
            }

            if(data.S21){
                self._updateChart(data.freq, data.S21, 'S21_goldenMatrix', {seriesName:'GoldenMatrix - S21', dashStyle:'ShortDash'});
            }
        },

        _updateChart: function(xData, yData, seriesId, options){
            var self = this;

            var seriesData = _.map(xData, function(value, index){
                var point = [value, yData[index]];
                return point;
            });

            // if series already exists, don't add, just update the data
            curSeries = self._chart.get(seriesId);
            if(curSeries){
                curSeries.setData(seriesData);
            } else {
                dashStyle = 'Solid';
                if(options.dashStyle){
                    dashStyle = options.dashStyle;
                }

                seriesName = '';
                if(options.seriesName){
                    seriesName = options.seriesName;
                }

                showInLegend = true;
                if(options.showInLegend != null && !options.showInLegend){
                    showInLegend = false;
                }

                enableTooltip = true;
                if(options.enableTooltip != null && !options.enableTooltip){
                    enableTooltip = false;
                } 

                pointMarker = false;
                if(options.pointMarker){
                    pointMarker = options.pointMarker;
                }

                var chartOptions = {
                    data: seriesData,
                    id: seriesId,
                    name: seriesName,
                    dashStyle: dashStyle,
                    showInLegend: showInLegend,
                    enableMouseTracking: enableTooltip,
                    marker: {
                        enabled: pointMarker
                    }
                };
                
                if(options.color){
                    $.extend(true, chartOptions, {color: options.color});
                }

                self._chart.addSeries(chartOptions);
            }
        },

        // this will remove specs that no longer exists
        _clearSpecificationInputs: function(specs){
            var self = this;
            // clear current 'cache of specs'
            _.each(self._loadedSpecificationInputs, function(spec){

                if(_.findWhere(specs, {id: spec.id})){
                    console.log('spec still exists: '+spec.id);
                    // it still exists in new list so don't remove it
                    return;
                }

                console.log('trying to clear: '+spec.id);
                curSeries = self._chart.get(spec.id);
                if(curSeries){
                    curSeries.remove(true);
                    console.log('cleared: '+spec.id);
                }
            });

            this._loadedSpecificationInputs = [];
        },

        _resetChart: function(){
            while( this._chart.series.length) {
                this._chart.series[0].remove(false);
            }

            this._chart.redraw();

            // redraw specification inputs            
        }
    };
});