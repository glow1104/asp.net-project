define(['underscore', 'matrixViewHelpers'], function(_, matrixViewHelpers){
    return {
        initialize: function(){
            var self = this;
            self.data = {};

            // create matrix container and button container
            self._$mainContainer = $('#extracted-matrix');
            self.initializeMatrixContainer();
            self._$matrixContainer = $('#extracted-matrix .matrix__matrix-container')
            
            // start listening for measurement data loaded
            $(window).on('extractedMatrix:loaded', function(e, data){
                self.data = data;
                matrixViewHelpers.loadData(data.matrixData, self._$matrixContainer);
            });

            $(window).on('sPlot:measurement:loaded', function(){
                // new measurement data was loaded so clear extracted matrix
                matrixViewHelpers.clearMatrix(self._$matrixContainer);
            });
        },

        initializeMatrixContainer: function(){
            this._$mainContainer.append("<div class='matrix__matrix-container form-group'></div>");
        }

    };
});