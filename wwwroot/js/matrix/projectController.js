define(function(){
    return {        
        initialize: function(){
            var self = this;

            $("#btn__save-project").click(function (evt) {
                // let get all the data required from the different 
                // sections
                
                // current measurement S11 and S12
                var measurementSPlot = $(window).triggerHandler('sPlotMeasurementData:get');
                
                // current golden matrix
                var measurementMatrix = $(window).triggerHandler('goldenMatrixData:get');
                
                // current topology
                var customTopology = $(window).triggerHandler('customTopology:get');
                
                // current filter information
                var filterInformation = $(window).triggerHandler('filterInformation:get');
                
                // current tuning
                var tuningParameters = $(window).triggerHandler('tuningParameters:get');
                
                // this data needs to match our Model on the server side
                var data = {
                    FilterOrder: filterInformation.filterOrder    
                };
                
                self.saveProjectSettings(data);
            });
        },
        
        saveProjectSettings: function(data){
            $.ajax({
                url: "/Matrix/SaveProject",
                method: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(data)
            }).done(function(data) {
                alert("Done: " + JSON.stringify(data));                         
            });            
        }
    };
});
