define(['underscore', 'topologyGraphViewer'], function(_, TopologyGraphViewer){
    function Topology(options){
        this._$typeInputElement = options.$typeInputElement;
        this._$orderInputElement = options.$orderInputElement;
        this._$containerElement = options.$containerElement;
    };
    
    // private static methods
        
    // this method will create a table of checkboxes
    // with the size of topologyOrder x topologyOrder
    function createCustomTopologyMap(order, $container){
        // clear the container
        $container.html('');
        
        // create the main container
        var $structureContainer = $('<div class="topology-custom__container"></div>');
        var $table = $('<table class="topology__table" style="border-collapse: collapse"></table>');
        for(var i = 0; i < order+2; i++){
            // need to add the column order #
            if(i === 0){
                var $headerRow = $('<tr class="topology__row--header"></tr>');
                for(var j = 0; j <= (order + 2); j++){
                    var orderStr = j-1;
                    if(j === 0){
                        orderStr = "";
                    }else if(j === 1){
                        orderStr = "S";
                    }else if(j === order + 2){
                        orderStr = "L";
                    }
                    
                    var $headerItem = $('<td class="topology__item--header"><span>' + orderStr + '</span></td>');
                    $headerRow.append($headerItem);
                }
                $table.append($headerRow);
            }
            
            // create row
            var $row = $('<tr class="topology__row"></tr>');
            for(var j = 0; j <= order+1; j++){
                // add row order
                if(j === 0){
                    var orderStr = i;
                    if(i === 0){
                        orderStr = "S";
                    }else if(i === order+1){
                        orderStr = "L";
                    }
                    var $headerItem = $('<td class="topology__item--header"><span>' + orderStr + '</span></td>');
                    $row.append($headerItem);
                }
                
                // create checkbox container
                var $ckboxContainer = $('<td class="topology__item"></td>');
                
                // create checkbox input
                var $ckbox = $('<span class="topology__check-item"><span>');
                var id = 'topology-' + j + '-' + i;
                $ckbox.attr('id', id);
                $ckbox.data('topology-x', j);
                $ckbox.data('topology-y', i);
                
                // check if the chkbox should be checked
                if(( j - 1 === i || j === i - 1 ) || (j === i && j !== 0 && j !== order + 1)){
                    $ckbox.addClass('glyphicon glyphicon-ok-sign').data('checked', true);
                }
                
                // append the checkbox to the checkbox container
                $ckboxContainer.append($ckbox);
                
                // append it to the row
                $row.append($ckboxContainer);
            }
            
            // append row to container
            $table.append($row);
        }
        
        // append the checkbox table to the structure container
        $structureContainer.append($table);

        // add to the main container
        $container.append($structureContainer);

        // get width of 1 item and make sure the container can hold them all
        var width = $('.topology__item').outerWidth();
        
        // total width is width of 1 item * (order + 3) where 1 is order # and 2 is for S and L        
        $structureContainer.css({width: (width * (order + 3))});
        
    }; // createCustomTopologyMap
    
    Topology.prototype = {
        constructor: Topology,

        ckboxClass: 'glyphicon glyphicon-ok-sign',
        
        initialize: function(){
            var self = this;
            self._topologyOrder = 0;

            // create topologyContainer and CT Number container
            self._$topologyContainer = $('<div id="topology__matrix-container"><span class="topology-msg">Please select a filter order between 1 - 20</span></div>');
            self._$ctNumberContainer = $('<div id="topology__ct-container"><span>CT=</span><span class="value">[]</span></div>)');
            self._$ctNumberContainer.hide();
            self._$graphContainerElement = $('<div id="matrix-topology-container"></div>')
            self._$containerElement.append(self._$ctNumberContainer).append(self._$topologyContainer).append(self._$graphContainerElement);
            
            // on filter order change, update the topology tab
            this._$orderInputElement.bind("propertychange change click keyup input paste", _.debounce(function(){
                    var newOrder = parseInt(self._$orderInputElement.val());
                    // if the order is different than what we have, create
                    // topology map again
                    if(!isNaN(newOrder) && self._topologyOrder != newOrder){

                        self._topologyOrder = newOrder; 
                        var matrix = self.createTopologyMap();
                        self.topologyGraphViewer.onMatrixChanged(matrix);
                    }
                }, 300) 
            );
            
            $(window).on('customTopology:get', function(e){
                return self.getCustomTopologyData();
            });

            self.topologyGraphViewer = new TopologyGraphViewer();

            self._$topologyContainer.on('click', '.topology__item', _.debounce(function(e){
                // get matrix
                var matrix = self._getStructureData();

                self.topologyGraphViewer.onMatrixChanged(matrix);
            }, 100));

        }, // initialize
        
        // factory method to decide which topology mao to create
        createTopologyMap: function(){
            var self = this;
            
            // 
            if(!isNaN(self._topologyOrder) && self._topologyOrder <= 20){
                createCustomTopologyMap(self._topologyOrder, self._$topologyContainer);
                
                // listen for checkbox changes as we want to keep them symmetric
                self._$topologyContainer.find('.topology__item').click(function(e){
                    var $ckbox = $(e.currentTarget).find('.topology__check-item');
                    var x = $ckbox.data('topology-x');
                    var y = $ckbox.data('topology-y');
                    var isChecked = $ckbox.data('checked'); 

                    // if x === y and x === 0 or order+1, then return
                    // as both M[S,S] and M[L,L] should stay 0
                    if(x === y && (x === 0 || x === self._topologyOrder + 1)){
                        return;
                    }

                    $ckbox.toggleClass(self.ckboxClass, !isChecked).data('checked', !isChecked);
                    // if the x and y are the same, don't need to do anything
                    // if they are different, we need to set the state of the 
                    // inverse checkbox to the same state as the clicked one
                    if(x !== y){
                        $inverseCkbox = $('#topology-' + y + '-' + x);
                        $inverseCkbox.toggleClass(self.ckboxClass, !isChecked).data('checked', !isChecked);
                    }

                    // Need to find the CT/CQ number
                    self._updateCtNumber();
                });

                // clear and show the ct number container
                //self._$ctNumberContainer.find('.value').html('')
                //self._$ctNumberContainer.show();

                return self._getStructureData();
            } else {
                // hide ct number container
                self._$ctNumberContainer.hide();
            }
            
            return {};
        }, // createTopologyMap

        _updateCtNumber: function(){
            var self = this;
            var ctArray = self.determineCtNumber();

            self._$ctNumberContainer.find('.value').text(JSON.stringify(ctArray));
        },

        // this method will determine the CT number
        // the following are the steps that will be taken
        // 1. Go through each row
        // 2. Get checked checkbox that isn't part of neutral zone
        // 3. Get number missing between the row, and the checked box column
        // 4. Store in array 
        //    determine number needed
        determineCtNumber: function(){
            var self = this;
            var ctArray = [];
            
            for(var row = 1; row < self._topologyOrder+2; row++){
                // neutral zone is row + 1, i.e. for row 3, columns 2, 3 and 4 are 
                // part of the neutral zone, so we need to start at column 5
                var minimumNonNeutral = row + 2;
                for(var col = minimumNonNeutral; col <= self._topologyOrder+2; col++ ){
                    isChecked = $('#topology-' + (row-1) + '-' + (col-1)).data('checked');

                    // (step 3.) if checked, then keep track of numbers 
                    // needed from row to this col
                    //console.log('ctcheck:#topology-' + (row-1) + '-' + (col-1) +'  col:'+col+'  row:'+row+ '   checked:'+isChecked);
                    if(isChecked){
                        for(var i = row + 1; i < col; i++){
                            // (step 4. add to array)
                            ctArray.push(i-1);
                        }
                    }
                }
            }
            // order and remove duplicates
            ctArray = _.uniq(ctArray.sort(function(a, b){return a - b;}));
            console.log('CT Array:' + ctArray);

            return ctArray
        }, // determineCtNumber
        
        _getStructureData: function(){
            var self = this;
            var structure = [];

            // go through the elements and create a 2 dimensional array            
            self._$topologyContainer.find('.topology__check-item').each(function(index){
                $element = $(this);
                x = $element.data('topology-x');
                y = $element.data('topology-y');
                isChecked = $element.data('checked');
                
                if(!structure[y]){
                    structure[y] = [];
                }
                
                structure[y][x] = isChecked ? 1 : 0;
            });
            
            return structure;
        },

        getCustomTopologyData: function(){
            var self = this;
            var customStructure = self._getStructureData();
            var ctArray = self.determineCtNumber();

            var data = {
                customStructure: customStructure,
                CT: ctArray
            }

            return data;
        }
    };
    
    return Topology;
});