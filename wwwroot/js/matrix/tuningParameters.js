define(function(){
    return {
        initialize: function(){
            var self = this;

            self._$FL = $('#tp__capture-from');
            self._$FH = $('#tp__capture-to');

            $(window).on('tuningParameters:get', function(e, options){
                var data = self._getAllData(options);
                return data;                
            });

            $(window).on('tuningParameters:getAllOrUndefined', function(e, options){
                var data = self._getAllData(options);
                if(self._isDataValid(data)){
                    return data;
                }
                
                return undefined;
            });
        },

        _getAllData: function(options){
            var self = this;
            if(!options){options = {}}
            
            var scale = 1;
            if(options.scale){
                scale = options.scale;
            }
            var FL = 0, FH = 0;

            // get value from form
            if(self._$FL.val()) FL = +self._$FL.val() * scale;
            if(self._$FH.val()) FH = +self._$FH.val() * scale;
            
            var data = {
                FL: FL,
                FH: FH
            };

            return data;
        },

        _isDataValid: function(data){
            // go through and make sure values are not null
            if (
                typeof data.FL !== 'undefined' &&
                typeof data.FH !== 'undefined'  
            ){
                return true;
            }

            return false;
        }
    };
});