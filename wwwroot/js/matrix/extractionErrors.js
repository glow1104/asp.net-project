define(['underscore', 'matrixViewHelpers'], function(_, matrixViewHelpers){
    return {

        initialize: function(){
            var self = this;
            self.data = null;
            self._errorThreshold = 0.001;
            self._errorChartData = null;

            self._$mainContainer = $('#error-matrix');
            self.initializeMatrixContainer();
            self._$matrixContainer = $('#error-matrix .matrix__matrix-container')

            // listen for threshold updates
            $(window).on('extractionErrors:threshold:updated', function(e, data){
                self._errorThreshold = data;
                self._updateErrorThresholdChart();
            });

            // listen for error matrix event
            $(window).on('extractionErrors:errorMatrix:loaded', function(e, data){
                self.data = data.errorMatrix;
                matrixViewHelpers.loadData(data.errorMatrix, self._$matrixContainer);
            });

            self._errorChart = self._initializeChart({
                $container: $('#error-chart'),
                title: 'Error Chart'
            });
            $(window).on('extractionErrors:errorChart:loaded', function(e, data){
                var categories = [];
                var yValues = [];

                // concatenate the errors
                // start is IO
                categories.push('Rin');
                categories.push('Rout');
                yValues.push(data.IO[0]);
                yValues.push(data.IO[1]);

                // ... continue with F
                _.each(data.F, function(errorPoint){
                    var label = 'M'+errorPoint.y+','+errorPoint.x;
                    categories.push(label);
                    yValues.push(errorPoint.value);
                });

                // ... continue with C
                _.each(data.C, function(errorPoint){
                    var label = 'M'+errorPoint.y+','+errorPoint.x;
                    categories.push(label);
                    yValues.push(errorPoint.value);
                });

                // ... continue with Zeros
                _.each(data.Zero, function(errorPoint){
                    var label = 'M'+errorPoint.y+','+errorPoint.x;
                    categories.push(label);
                    yValues.push(errorPoint.value);
                });

                self._errorChartData = {
                    categories: categories,
                    yValues: yValues
                };

                self._updateErrorThresholdChart();
            });

            // initialize freq error chart
            self._freqErrorChart = self._initializeChart({
                $container: $('#freq-errors'),
                title: 'Frequency Errors',
                chartOptions: {
                    chart: {
                        width: 400
                    }
                }
            });
            $(window).on('extractionErrros:freqErrors:loaded', function(e, data){
                self._updateChart(self._freqErrorChart, null, data.F, 'F', 'f-errors');
            });

            // initialize coupling error chart
            self._couplingErrorChart = self._initializeChart({
                $container: $('#coupling-errors'),
                title: 'Coupling Errors',
                chartOptions: {
                    chart: {
                        width: 400
                    }
                }
            });
            $(window).on('extractionErrros:couplingErrors:loaded', function(e, data){
                self._updateChart(self._couplingErrorChart, null, data.C, 'C', 'c-errors');
            });

            // initialize IO error chart
            self._ioErrorChart = self._initializeChart({
                $container: $('#io-errors'),
                title: 'IO Errors',
                chartOptions: {
                    chart: {
                        width: 200
                    }
                }
            });
            $(window).on('extractionErrros:ioErrors:loaded', function(e, data){
                self._updateChart(self._ioErrorChart, null, data.IO, 'IO', 'io-errors');
            });

            // initialize zero error chart
            self._zeroErrorChart = self._initializeChart({
                $container: $('#zero-errors'),
                title: 'Zero Errors',
                chartOptions: {
                    chart: {
                        width: 200
                    }
                }
            });
            $(window).on('extractionErrros:zeroErrors:loaded', function(e, data){
                self._updateChart(self._zeroErrorChart, null, data.Zero, 'Zero', 'zero-errors');
            });

            // initialize unloaded Q chart
            self._$unloadedQContainer = $('#unloaded-q');
            $(window).on('extractionErrros:unloadedQ:loaded', function(e, data){
                value = "";
                if(!isNaN(data.unloadedQ)){
                    value = 'Unloaded Q:' + data.unloadedQ.fixed(5);
                }
                self._$unloadedQContainer.text(value);
            });

            // listen for new measurement plot loaded
            $(window).on('sPlot:measurement:loaded', function(){
                // if data is null, return since there's no data
                // to reset
                if(self.data == null){
                    return;
                }

                // clear matrix
                matrixViewHelpers.clearMatrix(self._$matrixContainer);

                // reset highcharts (remove series)
                if(self._errorChart.series[0]){
                    self._errorChart.series[0].remove(true);
                }

                // reset unloaded Q
                self._$unloadedQContainer.text('');

                // reset data
                self.data = null;
            });

            $(window).on('extractionErrors:chart:resize', function(){
                self._errorChart.reflow();
            });            
        },

        initializeMatrixContainer: function(){
            this._$mainContainer.append("<div class='matrix__matrix-container form-group'></div>");
        },

        _initializeChart: function(options){
            var chartOptions = {
                chart: {
                    type: 'column'
                },
                credits: false,
                title: {
                    text: options.title
                },
                plotOptions: {
                    column: {
                        borderWidth: 0
                    }
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                }
            };

            if(options.chartOptions){
                $.extend(true, chartOptions, options.chartOptions);
            }
            options.$container.highcharts(chartOptions); 

            return options.$container.highcharts();            
        },

        _updateChart: function(chart, categories, yData, seriesId, seriesName){
            var self = this;

            // get the number of values for xAxis
            if(!categories){
                categories = _.map(yData, function(value, index){
                    return index + 1; 
                });
            } 
            chart.xAxis[0].categories = categories;

            // make sure all values are numbers
            yData = _.map(yData, function(value){
                var color = '#F00';
                if(Math.abs(+value) <= self._errorThreshold){
                    color = '#5B934A';
                }
                return {
                    y: +value,
                    color: color
                };
            });
            
            // if series already exists, don't add, just update the data
            curSeries = chart.get(seriesId);
            if(curSeries){
                curSeries.setData(yData);
            } else {
                chart.addSeries({
                    data: yData,
                    id: seriesId,
                    name: seriesName
                });
            }
        },

        _updateErrorThresholdChart: function(){
            if(!this._errorChartData){
                return;
            }
            
            this._errorChart.reflow();
            this._updateChart(this._errorChart, this._errorChartData.categories, this._errorChartData.yValues, 'Errors', 'Errors');
        },        
    };
});