define(function(){
    return {
        initialize: function(){
            var self = this;

            // cache elements
            self._$lossyModelSelector = $("input[name='tp-lossy-model']");

            // initialize change listener
            self._initializeChangeListener();

            $(window).on('lossyModel:get', function(e){
                return self._getLossyModel();
            });
        },

        _initializeChangeListener: function(){
            var self = this;

            // listen for changes to fband options
            self._$lossyModelSelector.change(_.debounce(function(){
                    // on change of the lossy model, broadcast event
                    // letting listeners know it has changed
                    $(window).trigger('lossyModel:changed', self._getLossyModel() == 0);
                }, 100)
            );
        },

        _getLossyModel: function(){
            var self = this;

            // get value from form
            var lossyModelStr = self._$lossyModelSelector.parent().find(':checked').val();
            var lossy = parseInt(lossyModelStr);      

            return lossy;
        }
    };
});