define(['underscore'], function(_){
    return {
        initialize: function($container){
            var self = this;
            self._$mainContainer = $container;

            // check for any input changes from the specification inputs
            // within the container
            self._$mainContainer.on("propertychange change click keyup input paste", "input", _.debounce( function(e){
                self._updateCharts();
                self._updateSpecificationInputs();
            }, 1500)); 
        },

        // check if the last iso has input
        _updateSpecificationInputs: function(){
            // check for Iso's
            // check if db's input exists, if it does
            // continue check next one, else break
            var i=1;
            var lastCompleted = 0;
            while($('#si-iso'+i+'__db').length > 0){
                result = this._getCompletedData('iso'+i);
                if(result){
                    lastCompleted = i;
                }
                i++;
            }

            // if the lastCompleted is the i-1, then
            // they are all entered, so create a new one
            if(lastCompleted === i-1){
                var template = this._getSpecificationInputTemplate(i);
                this._$mainContainer.append($(template));
            }
        },

        // check the inputs and update the splot for completed specifications
        _updateCharts: function(){
            var completedSpecs = [];

            // check for RL
            var result = this._getCompletedData('rl');
            if(result){
                completedSpecs.push(result);
            }

            // check for IL
            result = this._getCompletedData('il');
            if(result){
                completedSpecs.push(result);
            }

            // check for Iso's
            // check if db's input exists, if it does
            // continue check next one, else break
            var i=1;
            while($('#si-iso'+i+'__db').length > 0){
                result = this._getCompletedData('iso'+i);
                if(result){
                    completedSpecs.push(result);
                }
                i++;
            }

            // send data to who ever cares
            $(window).trigger('specificationInput:data:updated', {specs: completedSpecs});
        },

        // check the 3 inputs for the given spec type
        // if all 3 have values, send back the object
        // else return null
        _getCompletedData: function(type) {
            var dbId = '#si-' + type + '__db';
            var cfId = '#si-' + type + '__capture-from';
            var ctId = '#si-' + type + '__capture-to';

            var dbVal = parseFloat($(dbId).val());
            var cfVal = parseFloat($(cfId).val());
            var ctVal = parseFloat($(ctId).val());

            if(!dbVal || !cfVal || !ctVal){
                return null;
            }

            return {
                id: type,
                db: dbVal,
                cf: cfVal,
                ct: ctVal
            };
        },

        _getSpecificationInputTemplate: function(index){
            var inputTemplate = '';
            inputTemplate += '<div class="row">';
            inputTemplate += '    <div class="col-xs-12 form-group">';
            inputTemplate += '        <div>';
            inputTemplate += '            <span class="specification-input__title">Iso' + index + ' &lt;</span>';
            inputTemplate += '            <span class="input-group input-group-sm col-xs-7 pull-right">';
            inputTemplate += '                <input id="si-iso' + index + '__db" class="form-control input-sm" type="number" step="0.0001" placeholder=""/>';
            inputTemplate += '                <span class="input-group-addon" style="width:35px;">dB</span>';
            inputTemplate += '            </span>';
            inputTemplate += '        </div>';
            inputTemplate += '        <span class="input-group input-group-sm col-xs-12">';
            inputTemplate += '            <input id="si-iso' + index + '__capture-from" class="form-control input-sm" type="number" step="0.0001" placeholder="from"/>';
            inputTemplate += '            <span class="input-group-addon fband"></span>';
            inputTemplate += '        </span>';
            inputTemplate += '        <span class="input-group input-group-sm col-xs-12">';
            inputTemplate += '            <input id="si-iso' + index + '__capture-to" class="form-control input-sm" type="number" step="0.0001" placeholder="to"/>';
            inputTemplate += '            <span class="input-group-addon fband"></span>';
            inputTemplate += '        </span>';
            inputTemplate += '    </div>';
            inputTemplate += '</div>';      

            return inputTemplate;
        }
    }
});