define(['sortable', 'underscore'], function(Sortable, _){
    return {
        initialize: function(){
            var self = this;

            // initialize 
            self._originalFtz = null;

            // cache elements
            self._$N = $('#filter-order');

            self._initializeFrequencies();
            self._initializeSortOfZeros();

            self._$N.bind("propertychange change click keyup input paste", function(e, options){
                // make sure number is less than or equal to 25 or greater than 0
                var currentVal = parseInt(self._$N.val());

                // if currentVal is NaN, then user did not enter anything so leave it as is
                if(isNaN(currentVal)){
                    return;
                }

                if(currentVal < 1){
                    currentVal = 1;
                } else if(currentVal > 20){
                    currentVal = 20;
                }

                self._$N.val(currentVal);
            });

            $('#sort-of-zeros').on("propertychange change click keyup input paste", "input", _.debounce( function(e){
                self._updateSortOfZerosInputs();
            }, 250)); 

            $(window).on('filterInformation:get', function(e, options){
                var data = self._getAllData(options);

                return data;
            });

            $(window).on('filterInformation:getAllOrUndefined', function(e, options){
                var data = self._getAllData(options);
                if(self._isDataValid(data)){
                    return data; 
                }
                
                return undefined
            });

            $(window).on('filterInformation:ftz:loaded', function(e, data){
                self._loadFtzData(data);
            })
        },

        _initializeFrequencies: function(){
            var self = this;

            // listen for changes to start/stop/center freq and bandwidth
            // only need 2 to get other 2 so for each change
            // check values and determine the other 2
            self._$f1 = $('input#filter-f1');
            self._$f2 = $('input#filter-f2');
            self._$f0 = $('input#filter-f0');
            self._$bw = $('input#filter-bw');
            self._$f1.blur(_.debounce(function(e){
                    // get the scale to work with actual values to reduce floating point errors
                    var scale = $(window).triggerHandler('frequencyScale:get');

                    // start freq has changed so check the following in the specified
                    // order to determine 2 others
                    // 1. if stop freq has a value, calculate f0 and bw
                    // 2. if centre freq has a value, calculate f2 and bw
                    // 3. if bw has a value, calculate f0 and f2
                    var fValues = self._getFreqValues();
                    var tmpf1 = fValues.f1 != null ? fValues.f1 * scale : null;
                    var tmpf2 = fValues.f2 != null ? fValues.f2 * scale : null;
                    var tmpf0 = fValues.f0 != null ? fValues.f0 * scale : null;
                    var tmpbw = fValues.bw != null ? fValues.bw * scale : null;
                    var f2, f0, bw;
                    if(tmpf2 != null){
                        bw = tmpf2 - tmpf1;
                        f0 = tmpf1 + (bw/2);
                        f2 = tmpf2;
                    } else if(tmpf0 != null){
                        bw = (tmpf0 - tmpf1) * 2;
                        f2 = tmpf1 + bw;  
                        f0 = tmpf0;
                    } else if(tmpbw != null){
                        f2 = tmpf1 + bw;
                        f0 = tmpf1 + (bw/2);
                        bw = tmpbw;
                    }
                    if(f2) self._$f2.val(f2 / scale);
                    if(f0) self._$f0.val(f0 / scale);
                    if(bw) self._$bw.val(bw / scale);
                }, 250)
            );
            $('input#filter-f2').blur(_.debounce(function(e){
                    // get the scale to work with actual values to reduce floating point errors
                    var scale = $(window).triggerHandler('frequencyScale:get');

                    // stop freq has changed so check the following in the specified
                    // order to determine 2 others
                    // 1. if start freq has a value, calculate f0 and bw
                    // 2. if centre freq has a value, calculate f1 and bw
                    // 3. if bw has a value, calculate f0 and f1
                    var fValues = self._getFreqValues();
                    var tmpf1 = fValues.f1 != null ? fValues.f1 * scale : null;
                    var tmpf2 = fValues.f2 != null ? fValues.f2 * scale : null;
                    var tmpf0 = fValues.f0 != null ? fValues.f0 * scale : null;
                    var tmpbw = fValues.bw != null ? fValues.bw * scale : null;
                    var f1, f0, bw;
                    if(tmpf1 != null){
                        bw = tmpf2 - tmpf1
                        f0 = tmpf1 + bw/2;
                        f1 = tmpf1;
                    } else if(tmpf0 != null){
                        bw = (tmpf2 - tmpf0) * 2;
                        f1 = tmpf2 - bw;  
                        f0 = tmpf0;
                    } else if(tmpbw != null){
                        f1 = tmpf2 - bw;
                        f0 = tmpf2 - (bw/2);
                        bw = tmpbw;
                    }
                    if(f1) self._$f1.val(f1 / scale);
                    if(f0) self._$f0.val(f0 / scale);
                    if(bw) self._$bw.val(bw / scale);
                }, 250)
            );
            $('input#filter-f0').blur(_.debounce(function(e){
                    // get the scale to work with actual values to reduce floating point errors
                    var scale = $(window).triggerHandler('frequencyScale:get');

                    // center freq has changed so check the following in the specified
                    // order to determine 2 others
                    // 1. if bw has a value, calculate f1 and f2
                    // 2. if start freq has a value, calculate f2 and bw
                    // 3. if stop freq has a value, calculate f1 and bw
                    var fValues = self._getFreqValues();
                    var tmpf1 = fValues.f1 != null ? fValues.f1 * scale : null;
                    var tmpf2 = fValues.f2 != null ? fValues.f2 * scale : null;
                    var tmpf0 = fValues.f0 != null ? fValues.f0 * scale : null;
                    var tmpbw = fValues.bw != null ? fValues.bw * scale : null;
                    var f1, f2, bw;
                    if(tmpbw != null){
                        f1 = tmpf0 - (tmpbw/2);
                        f2 = tmpf0 + (tmpbw/2);
                        bw = tmpbw;
                    } else if(tmpf1 != null){
                        bw = (tmpf0 - tmpf1) * 2
                        f2 = tmpf1 + bw;
                        f1 = tmpf1;
                    } else if(tmpf2 != null){
                        bw = (tmpf2 - tmpf0) * 2
                        f1 = tmpf2 - bw;
                        f2 = tmpf2;
                    }
                    if(f1) self._$f1.val(f1 / scale);
                    if(f2) self._$f2.val(f2 / scale);
                    if(bw) self._$bw.val(bw / scale);
                }, 250)
            );
            $('input#filter-bw').blur(_.debounce(function(e){
                    // get the scale to work with actual values to reduce floating point errors
                    var scale = $(window).triggerHandler('frequencyScale:get');

                    // bw has changed so check the following in the specified
                    // order to determine 2 others
                    // 1. if centre freq has a value, calculate f1 and f2
                    // 2. if start freq has a value, calculate f2 and f0
                    // 3. if stop freq has a value, calculate f1 and f0
                    var fValues = self._getFreqValues();
                    var tmpf1 = fValues.f1 != null ? fValues.f1 * scale : null;
                    var tmpf2 = fValues.f2 != null ? fValues.f2 * scale : null;
                    var tmpf0 = fValues.f0 != null ? fValues.f0 * scale : null;
                    var tmpbw = fValues.bw != null ? fValues.bw * scale : null;
                    var f1, f2, bw;

                    if(tmpf0 != null){
                        f1 = tmpf0 - (tmpbw/2);
                        f2 = tmpf0 + (tmpbw/2);
                        f0 = tmpf0;
                    } else if(tmpf1 != null){
                        f0 = tmpf1 + (tmpbw/2)
                        f2 = tmpf1 + tmpbw;
                        f1 = tmpf1;
                    } else if(tmpf2 != null){
                        f0 = tmpf2 - (tmpbw/2)
                        f1 = tmpf2 - tmpbw;
                        f2 = tmpf2;
                    }
                    if(f1) self._$f1.val(f1 / scale);
                    if(f2) self._$f2.val(f2 / scale);
                    if(f0) self._$f0.val(f0 / scale);
                }, 250)
            );
        },

        _initializeSortOfZeros: function() {
            var sozElement = $('#sort-of-zeros')[0];

            Sortable.create(sozElement, {
                handle: '.glyphicon-sort'
            });
        },

        _updateSortOfZerosInputs: function() {
            // go through the list of sort zeros up to the filter order
            var filterOrder = parseInt(this._$N.val());
            var lastItemHasValue = false;
            var lastItemIndex = -1;

            var $itemInputs = $('#sort-of-zeros').children();

            // first check if need to be deleted (from the end)
            // if there is more than 2 inputs, check the last and last-1 
            // and if they are both 'empty', remove the last one
            // and reset their IDs
            var itemInputCount = $itemInputs.length 
            if(itemInputCount > 1){
                var deleteCount = 0;
                for(var i = 1; i <= itemInputCount; i++){
                    var $element = $($itemInputs.get(-i));

                    var $realElement = $element.find("[data-type='real']");
                    var $imgElement = $element.find("[data-type='imaginary']");
                    // get values for real and imaginary parts
                    var realVal = parseFloat($realElement.val());
                    var imgVal = parseFloat($imgElement.val());

                    if(isNaN(realVal) && isNaN(imgVal)){
                        deleteCount = i - 1;
                    } else {
                        break;
                    }
                }

                var $itemLabels = $('#sort-of-zeros-container .item-labels').children();
                for(var i = 1; i <= deleteCount; i++){
                    // remove label
                    $element = $($itemLabels.get(-i));
                    $element.remove();

                    // remove input
                    $element = $($itemInputs.get(-i));
                    $element.remove();
                }
            } 

            $itemInputs.each(function(index, element){
                $element = $(element);

                var $realElement = $element.find("[data-type='real']");
                var $imgElement = $element.find("[data-type='imaginary']");
                // get values for real and imaginary parts
                var realVal = parseFloat($realElement.val());
                var imgVal = parseFloat($imgElement.val());

                if( (!isNaN(realVal) && realVal !== 0) || (!isNaN(imgVal) && imgVal !== 0)){
                    // since there's a value, mark that the last checked item
                    // has a value
                    lastItemHasValue = true;
                    lastItemIndex = index + 1;
                } else {
                    lastItemHasValue = false;
                }
            });

            // if last item has a value, and lastItemIndex <= filterOrder
            // add another input row
            if(lastItemHasValue && lastItemIndex < filterOrder){
                var htmlTemplate = this._getSortOfZerosItemHtml(lastItemIndex + 1);
                var labelHtml = $(htmlTemplate.label);
                var inputHtml = $(htmlTemplate.input);

                // add them to the view
                $('#sort-of-zeros-container .item-labels').append(labelHtml);
                $('#sort-of-zeros-container .item-inputs').append(inputHtml);
            }            
        },

        _getSortOfZerosItemHtml: function(index){
            var labelTemplate = '';
            labelTemplate += '<span id="fi__zero-freq--f' + index + '-label" class="input-group-addon column-form-addon" disabled>f' + index + '=</span>';

            var inputTemplate = '';
            inputTemplate += '<span class="input-group input-group-sm">';
            inputTemplate += '    <input id="fi__zero-freq--f' + index + '-r" class="form-control" data-type="real" type="number" step="0.1" placeholder="real"/>';
            inputTemplate += '    <span class="input-group-addon"></span>';
            inputTemplate += '    <input id="fi__zero-freq--f' + index + '-i" class="form-control" data-type="imaginary" type="number" step="0.1" placeholder="imaginary"/>';
            inputTemplate += '    <span id="fi__zero-freq--f' + index + '-handle" class="input-group-addon" disabled><span class="glyphicon glyphicon-sort  grabbable"></span></span>';
            inputTemplate += '</span>';

            return {
                label: labelTemplate,
                input: inputTemplate
            };
        },
            
        _getAllData: function(options){
            var self = this;
            if(!options){options = {}}

            var scale = 1;
            if(options.scale){
                scale = options.scale;
            }

            // for returned data;
            var filterOrder, f0, bw, Z;

            // get value from form
            if (self._$N.val()) filterOrder = +self._$N.val();
            if (self._$f0.val()) f0 = +self._$f0.val() * scale;
            if (self._$bw.val()) bw = +self._$bw.val() * scale;

            Z = self._getZeroData();

            var data = {
                N: filterOrder,
                f0: f0,
                bw: bw,
                Z: Z
            };

            return data;
        },

        _getZeroData: function(){
            var freqZeros = [];
            var $itemInputs = $('#sort-of-zeros').children();

            $itemInputs.each(function(index, element){
                $element = $(element);

                var $realElement = $element.find("[data-type='real']");
                var $imgElement = $element.find("[data-type='imaginary']");
                // get values for real and imaginary parts
                var realVal = parseFloat($realElement.val());
                var imgVal = parseFloat($imgElement.val());

                var freqZero = [];
                var push = true;
                if(!isNaN(realVal) || !isNaN(imgVal)){
                    freqZero.push(isNaN(realVal) ? 0 : realVal);
                    freqZero.push(isNaN(imgVal) ? 0 : imgVal);
                }else if(index === $itemInputs.length - 1){
                    push = false;
                }
                if(push){
                    freqZeros.push(freqZero);
                }
            });

            return freqZeros;
        },

        _isDataValid: function(data){
            // go through and make sure values are not null
            if (
                typeof data.N !== 'undefined' &&
                typeof data.f0 !== 'undefined' &&
                typeof data.bw !== 'undefined' 
            ){
                return true;
            }

            return false;
        },

        _getFreqValues: function(){
            var f1 = f2 = f0 = bw = null;
            if(this._$f1.val() != ''){
                f1 = parseFloat(this._$f1.val());
            }
            if(this._$f2.val() != ''){
                f2 = parseFloat(this._$f2.val());
            }
            if(this._$f0.val() != ''){
                f0 = parseFloat(this._$f0.val());
            }
            if(this._$bw.val() != ''){
                bw = parseFloat(this._$bw.val());
            }

            return {
                f1: f1,
                f2: f2,
                f0: f0,
                bw: bw
            }
        }
    };
});