define(function(){
    return {
        selfy: this,

        initialize: function(){
            var self = this;

            self._$qu = $('#mi__qu--input');
            self._quInfoMessage = '';
            // set tooltip
            $('#mi__qu--input-info').tooltip({
                title: self._getQuInfoMessage.bind(self)
            });

            $(window).on('matrixInformation:getAllOrUndefined', function(e, options){
                var data = self._getAllData(options);
                if(self._isDataValid(data)){
                    return data;
                }

                return undefined
            });

            $(window).on('matrixInformation:unloadedQ:loaded', function(e, data){
                $('#mi__unloaded-qu--input').val(data.q.toFixed(3));
            });

            $(window).on('matrixInformation:get', function(e, options){
                var data = self._getAllData(options);
            });

            self._$qu.bind("propertychange change click keyup input paste", _.debounce(function(e){
                // warn the user is the value is less than 200
                var value = self._$qu.val();
                var numericValue = parseInt(value);

                if(numericValue < 200){
                    $('#mi__qu--input-info').show();
                    self._$qu.parent().addClass('has-warning');
                    self._$qu.parent().removeClass('has-error');
                    self._quInfoMessage = "Qu should be more than 200!";
                } else if(value != "" && isNaN(numericValue)){
                    $('#mi__qu--input-info').show();
                    self._$qu.parent().addClass('has-error');
                    self._$qu.parent().removeClass('has-warning');
                    self._quInfoMessage = "Qu should be a number above 200";
                } else {
                    $('#mi__qu--input-info').hide();
                    self._$qu.parent().removeClass('has-warning has-error');
                    self._quInfoMessage = "";    
                }
            }, 250));

            // show error message on mouseover
        },

        _getQuInfoMessage: function() {
            return this._quInfoMessage;
        },

        _getAllData: function() {
            var self = this;

            var qu = -1;
            // get value from form
            if (self._$qu.val()){ 
                qu = +self._$qu.val();
            }

            var data = {
                qu: qu
            };
            
            return data;            
        },

        _isDataValid: function(data){
            // go through and make sure values are not null
            if (typeof data.qu !== 'undefined'){
                return true;
            }

            return false;
        }
    };
});