define(['underscore', 'mathJs'], function(_, mathJs){
    return {
        initialize: function(){
            var self = this;

            self._initializeKeyEventHandler();

            $('#btn--extract-matrix').click(function(){
                self.extractMatrix();
            });
        },

        _initializeKeyEventHandler: function() {
            $(document).keyup(function (event){
                if(event.which === 69){
                    // trigger extract clicked
                    $("#btn--extract-matrix").trigger('click');
                }
            });            
        },
        

        extractMatrix: function(){
            var self = this;

            // get scale
            var scale = $(window).triggerHandler('frequencyScale:get');

            // set data options
            var options = {
                scale: scale
            };

            // current Measurement Plot's data
            var sPlotMeasurementData = $(window).triggerHandler('sPlot:measurement:get');

            // current golden matrix
            $(window).triggerHandler('goldenMatrixData:validate');
            var goldenMatrix = $(window).triggerHandler('goldenMatrixData:get');
            
            // current topology
            $(window).triggerHandler('customTopology:validate');
            var customTopology = $(window).triggerHandler('customTopology:get');
            
            // get matrix data
            $(window).triggerHandler('matrixInformation:validate');
            var matrixInformation = $(window).triggerHandler('matrixInformation:getAllOrUndefined', options);

            // current filter information
            $(window).triggerHandler('filterInformation:validate');
            var filterInformation = $(window).triggerHandler('filterInformation:getAllOrUndefined', options);
            
            // current tuning
            $(window).triggerHandler('tuningParameters:validate');
            var tuningParameters = $(window).triggerHandler('tuningParameters:getAllOrUndefined', options);

            var errorMsg = "";
            if(!sPlotMeasurementData){
                errorMsg += "sPlot Data file must be uploaded<br />";
            }
            if(!matrixInformation){
                errorMsg += "Matrix Information must be entered<br />";
            }
            if(!filterInformation){
                errorMsg += "Filter Information must be entered<br />";
            }
            if(!tuningParameters){
                errorMsg += "Tuning Settings must be entered<br />";
            }
            if(errorMsg.length > 0){
                bootbox.alert(errorMsg);
                return;
            }

            //var parameterData = $.extend(true, dataFilename, goldenMatrix, customTopology, matrixInformation, filterInformation, tuningParameters);

            options = {
                success: function (data) {
                    // parse the json
                    var jsonObject = JSON.parse(data);
                    $(window).trigger('matrixExtractionData:loaded', jsonObject);
                    processingModal.modal('hide');
                    if(jsonObject.processTime){
                        bootbox.dialog({
                            message: 'Process Time to extract the matrix: ' + ( Math.round(jsonObject.processTime*1000)/1000 + ' seconds' ),
                            title: 'Extracting Matrix...'
                        });
                    }
                },
                error: function () {
                    processingModal.modal('hide');
                    bootbox.dialog({
                        message: 'There was an error extracting the matrix.  Please make sure the given information is correct',
                        title: 'Extracting Matrix...'
                    });
                }
            };

            // get f0 and bw in GHz scale
            var f0 = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.f0, originalScale: 'Hz', scale: 'GHz'});
            var bw = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.bw, originalScale: 'Hz', scale: 'GHz'});
            var FL = $(window).triggerHandler('frequencyScale:getValueForScale', {value: tuningParameters.FL, originalScale: 'Hz', scale: 'GHz'});
            var FH = $(window).triggerHandler('frequencyScale:getValueForScale', {value: tuningParameters.FH, originalScale: 'Hz', scale: 'GHz'});
            var targetMatrix = goldenMatrix.goldenMatrix
            if(targetMatrix.length === 0){
                targetMatrix = (mathJs.zeros(filterInformation.N + 2, filterInformation.N + 2))._data;
            }

            var parameterData = {
                freq: sPlotMeasurementData.freq,
                S11_db: sPlotMeasurementData.S11_db,
                S11_angRad: sPlotMeasurementData.S11_angRad,
                S21_db: sPlotMeasurementData.S21_db,
                S21_angRad: sPlotMeasurementData.S21_angRad,
                tranZeros: filterInformation.Z,
                filterOrder: filterInformation.N,
                centerFreq: f0,
                bandwidth: bw,
                captureStartFreqGHz: FL,
                captureStopFreqGHz: FH,
                isSymmetric: false,
                topology: customTopology.customStructure,
                targetMatrix: targetMatrix
            };

            var cacheBuster = (new Date()).getTime();
            var ajaxOptions = {
                url: '/Matrix/ExtractMatrix?t='+cacheBuster,
                type: "POST",
                processData: false,
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(parameterData)
            };


            // open modal letting user know that it is processing
            var processingModal = bootbox.dialog({
                message: 'Please wait while we extract your matrix',
                title: 'Extracting Matrix...',
                onEscape: false,
                closeButton: false,
                buttons: {}
            });

            // combine options
            ajaxOptions = $.extend(true, ajaxOptions, options)
            $.ajax(ajaxOptions);
        }
    };
});        