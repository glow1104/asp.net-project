define(['underscore', 'matrixHelpers'], function(_, matrixHelpers){
    return {
        initialize: function(){
            var self = this;
            self.data = {
                goldenMatrix: []
            };

            // create matrix container and button container
            self._$mainContainer = $('#golden-matrix');
            self.initializeMatrixContainer();
            self.initializeButtonContainer();
            
            // start listening for measurement data loaded
            $(window).on('goldenMatrix:loaded', function(e, data){
                self.data = data;
                self.loadData(data.goldenMatrix);

                bootbox.confirm({
                    message: "Do you want to set this matrix as the Golden Matrix?",
                    buttons: {
                        confirm: {
                            label: 'Yes'
                        },
                        cancel: {
                            label: 'No'
                        }
                    },
                    callback: function (result) {
                        if(result){
                            self._$setGoldenMatrixButton.prop('disabled', 'disabled');
                        } else {
                            self._$setGoldenMatrixButton.prop('disabled', false);
                        }
                    }
                });
                
                self._$generateSPlotButton.prop('disabled', false);
            });

            $('.golden-matrix__matrix-container').on('change', '.matrix__value', function(e){
                // there has been a change, so couple things to do:
                // 1. disable the splot and re-enable the 'set as golden matrix' button.
                self._$setGoldenMatrixButton.prop('disabled', false);

                // 2. update the diagonal number
                var $updatedValue = $(e.currentTarget)
                var x = $updatedValue.data('topology-x');
                var y = $updatedValue.data('topology-y');

                // if the x and y are the same, don't need to do anything
                // if they are different, we need to set the value of the 
                // inverse input to the same value as the updated one
                if(x !== y){
                    $inverseValue = $('#golden-matrix-' + x + '-' + y);
                    $inverseValue.val($updatedValue.val());
                }
                
            });
            
            $(window).on('goldenMatrixData:get', function(e){
                return {
                    goldenMatrix: self.data.goldenMatrix
                };
            });            
        },

        initializeMatrixContainer: function(){
            var self = this;
            self._$mainContainer.append("<div class='golden-matrix__matrix-container form-group'></div>");
        },

        initializeButtonContainer: function(){
            var self = this;

            var $buttonContainer = $("<div class='golden-matrix__button-container btn-selection'></div>");
            var buttonHtml = " \
                <div class=\"row btn-selection text-center\"> \
                    <input id=\"btn--generate-splot\" type=\"button\" class=\"btn btn-primary\" value=\"Generate S-Plot\" disabled/> \
                    <input id=\"btn--set-as-golden-matrix\" type=\"button\" class=\"btn btn-primary\" value=\"Set As Golden Matrix\" disabled/> \
                </div>";
            
            $buttonContainer.append(buttonHtml);
            
            self._$mainContainer.append($buttonContainer);

            self._$generateSPlotButton = $('#btn--generate-splot');
            self._$generateSPlotButton.click(self._generateSPlotClicked.bind(self));

            self._$setGoldenMatrixButton = $('#btn--set-as-golden-matrix');
            self._$setGoldenMatrixButton.click(self._setGoldenMatrixClicked.bind(self));
        },

        _setGoldenMatrixClicked: function(e) {
            // update golden matrix button
            this._$setGoldenMatrixButton.prop('disabled', 'disabled');

            // set the data to the new data
            data = this._getStructureData();

            this.data.goldenMatrix = data;
        },

        _getStructureData: function(){
            var self = this;
            var structure = [];

            // go through the elements and create a 2 dimensional array            
            self._$mainContainer.find('.matrix__value').each(function(index){
                $element = $(this);
                x = $element.data('topology-x');
                y = $element.data('topology-y');
                
                if(!structure[y]){
                    structure[y] = [];
                }
                
                structure[y][x] = parseFloat($element.val());
            });
            
            return structure;
        },        

        _generateSPlotClicked: function(e){
            var self = this;

            // get the scale
            // get scale
            var scale = $(window).triggerHandler('frequencyScale:get');

            // set data options
            var options = {
                scale: scale
            };
            
            // get optional data 
            
            // including the measurement splot - for freq
            var sPlotMeasurementData = $(window).triggerHandler('sPlot:measurement:get');

            // including the tuning parameters to find the freq 
            // in case splot mesurement data is not given
            var tuningParameters = $(window).triggerHandler('tuningParameters:getAllOrUndefined', options);


            // need to make sure all required data is filled

            // including GoD, matrix info and filter info
            var goldenMatrix = $(window).triggerHandler('goldenMatrixData:get');

            // current matrix information
            var matrixInformation = $(window).triggerHandler('matrixInformation:getAllOrUndefined', options);

            // current filter information
            var filterInformation = $(window).triggerHandler('filterInformation:getAllOrUndefined', options);

            var errorMsg = "";
            if(!goldenMatrix){
                errorMsg += "Golden Matrix must be uploaded<br />";
            }
            if(!matrixInformation){
                errorMsg += "Matrix Information must be entered<br />";
            }
            if(!filterInformation){
                errorMsg += "Filter Information must be entered<br />";
            }

            if(errorMsg.length > 0){
                bootbox.dialog({
                    message: errorMsg,
                    title: 'Errors Plotting Golden Matrix...'
                });
                return;
            }

            // if no sPlotMeasurementData, then figure out the x data
            // for 800 points from filter information
            var sPlotMeasurementData = $(window).triggerHandler('sPlot:measurement:get');
            var freqGHz = [];
            if(!sPlotMeasurementData){
                // there is no splot data so take f1 + (i * bw/800)

                var bw = filterInformation.bw * 3;
                var f1 = filterInformation.f0 - (bw /2 );
                // if tuning parameters is set, then set f1 to FL
                // and update bw
                if(tuningParameters && tuningParameters.FL > 0 && tuningParameters.FH > 0){
                    f1 = tuningParameters.FL;
                    bw = tuningParameters.FH - tuningParameters.FL; 
                }
                for(var i = 0; i < 801; i++){
                    var freq = f1 + (i * (bw/800));
                    
                    // convert to GHz
                    freq = $(window).triggerHandler('frequencyScale:getValueForScale', {value: freq, originalScale: 'Hz', scale: 'GHz'});

                    freqGHz.push(freq);
                }
            } else {
                freqGHz  = _.map(sPlotMeasurementData.freq, function(value){
                    return value / 1e3;
                });
            }

            // get data for sPlot
            var freqMHz = _.map(freqGHz, function(value){
                return $(window).triggerHandler('frequencyScale:getValueForScale', {value: value, originalScale: 'GHz', scale: 'MHz'});
            });

            var qu = matrixInformation.qu;
            if(qu === -1){
                qu = Infinity;
            }

            // get f0 and bw in GHz scale
            var f0 = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.f0, originalScale: 'Hz', scale: 'GHz'});
            var bw = $(window).triggerHandler('frequencyScale:getValueForScale', {value: filterInformation.bw, originalScale: 'Hz', scale: 'GHz'});

            splotData = matrixHelpers.CM2S(goldenMatrix.goldenMatrix, freqGHz, qu, f0, bw);
            var S11 = splotData.S11.map(function(s){return 10 * Math.log(s[1].x * s[1].x + s[1].y * s[1].y) / Math.LN10});
            var S21 = splotData.S21.map(function(s){return 10 * Math.log(s[1].x * s[1].x + s[1].y * s[1].y) / Math.LN10});
            var goldenMatrixSPlotData = {
                freq: freqMHz,
                S11: S11,
                S21: S21
            }
            
            $(window).trigger('sPlot:goldenMatrix:loaded', goldenMatrixSPlotData);
        },
        
        loadData: function(goldenMatrix){
            var self = this;

            $mainContainer = $('#golden-matrix .golden-matrix__matrix-container');
            
            var order = goldenMatrix.length;

            // create container
            var $goldenMatrix = $('<div class="golden-matrix__container"></div>');
            
            // create table container            
            var $table = $('<div class="matrix__table"></div>');
            
            // create data items
            // for each item in the golden matrix (rows), there is another array (columns)
            _.each(goldenMatrix, function(matrixRow, rowIndex){
                // if first item, then create top row for order
                if(rowIndex === 0){
                    var $headerRow = $('<div class="matrix__row--header"></div>');
                    for(var j = 0; j < (order + 1); j++){
                        var orderStr = j - 1;
                        if(j === 0){
                            orderStr = '';
                        }else if(j === 1){
                            orderStr = 'S';
                        }else if(j === order){
                            orderStr = 'L';
                        }
                        
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $headerRow.append($headerItem);
                    }
                    $table.append($headerRow);
                }
                
                // create row
                var $row = $('<div class="matrix__row"></div>');
                // go through each value
                _.each(matrixRow, function(matrixItem, colIndex){
                    // add row order at begining 
                    if(colIndex === 0){
                        var orderStr = rowIndex;
                        if(rowIndex === 0){
                            orderStr = 'S';
                        }else if(rowIndex === (order - 1)){
                            orderStr = 'L';
                        }
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $row.append($headerItem);
                    }

                    // determine level index
                    var levelIndex = Math.abs(rowIndex - colIndex)
                    if(levelIndex > 0){
                        if(matrixItem == 0){
                            levelIndex = '';
                        } else if(levelIndex > 2){
                            levelIndex = 'x';
                        }
                    }
                    
                    // create checkbox container
                    var $valueContainer = $('<div class="matrix__item level-index--' + levelIndex + '"></div>');
                    
                    // create checkbox input
                    var $value = $('<input type="number" class="matrix__value"></input>');
                    $value.val(matrixItem);
                    var id = 'golden-matrix-' + rowIndex + '-' + colIndex;
                    $value.attr('id', id);
                    $value.data('topology-x', colIndex);
                    $value.data('topology-y', rowIndex);
                    
                    // append the checkbox to the checkbox container
                    $valueContainer.append($value);
                    
                    // append it to the row
                    $row.append($valueContainer);
                    
                    // add row order at end 
                    if(colIndex === (order - 1)){
                        var orderStr = rowIndex;
                        if(rowIndex === 0){
                            orderStr = 'S';
                        }else if(rowIndex === (order - 1)){
                            orderStr = 'L';
                        }
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $row.append($headerItem);
                    }
                    
                });
                
                // append row to container
                $table.append($row);
                
                // if first item, then create top row for order
                if(rowIndex === (order - 1)){
                    var $headerRow = $('<div class="matrix__row--header"></div>');
                    for(var j = 0; j < (order + 1); j++){
                        var orderStr = j - 1;
                        if(j === 0){
                            orderStr = '';
                        }else if(j === 1){
                            orderStr = 'S';
                        }else if(j === order){
                            orderStr = 'L';
                        }
                        
                        var $headerItem = $('<div class="matrix__item--header"><span>' + orderStr + '</span></div>');
                        $headerRow.append($headerItem);
                    }
                    $table.append($headerRow);
                }                                
                
            });
            
            // add table
            $goldenMatrix.append($table);
            
            // add to dom element
            $mainContainer.html($goldenMatrix);
            
            // get width of 1 item and make sure the container can hold them all
            var width = $('.matrix__item').outerWidth();
            
            // total width is width of 1 item * (order + 1) where 1 is order #        
            $goldenMatrix.css({width: (width * (order + 2))});

        },

        updateButtons: function(){

        }
    };
});