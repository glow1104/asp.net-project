define(function(){
    return {
        initialize: function(){
            var self = this;

            // cache elements
            self._$freqScaleOptions = $("input[name='fband-options']");

            // cache selected value
            self.fScale = self._$freqScaleOptions.parent().find(':checked').val();

            // initialize change listener
            self._initializeChangeListener();

            $(window).on('frequencyScale:get', function(e){
                return self._getFrequencyScale();
            });

            $(window).on('frequencyScale:getValueForScale', function(e, options){
                return self._getValueForSpecifiedScale(options);
            });
        },

        _initializeChangeListener: function(){
            var self = this;

            // listen for changes to fband options
            self._$fbandSelector = $("input[name='fband-options']");
            self._$fbandSelector.change(_.debounce(function(){
                    // on change of the fband options,
                    // we want to update the value of the parameters that
                    // are based on the fband
                    self.fScale = $("input[name='fband-options']:checked").val();
                    $('.fband').text(self.fScale);
                }, 100)
            );
        },

        _getFrequencyScale: function(){
            return this._getScaleValue(this.fScale);
        },

        _getValueForSpecifiedScale: function(options){
            var value = options.value;
            var newScaleValue = this._getScaleValue(options.scale);
            var originalScale = this.fScale;
            if(options.originalScale){
                originalScale = options.originalScale;
            } 
            
            originalScaleValue = this._getScaleValue(originalScale);

            newValue = value * (originalScaleValue/newScaleValue);

            return newValue;
        },

        _getScaleValue: function(scaleStr){
            var scale = 1;

            switch(scaleStr){
                case 'kHz':
                    scale = 1000;
                    break;
                case 'MHz':
                    scale = 1000000;
                    break;
                case 'GHz':
                    scale = 1000000000;
                    break;
            }

            return scale;
        }
    };
});