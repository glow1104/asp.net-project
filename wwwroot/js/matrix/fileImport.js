
define(['matrixHelpers', 'underscore'], function(matrixHelpers, _){
    return {
        initialize: function(){
            var self = this;
            self.dataFilename = null;
            self.matrixFilename = null;

            self._initializeFileReader();
            self._initializeKeyEventHandler();

            $('.btn-file input:file').on('fileselect', function(event, numFiles, label) {
                var inputId = event.currentTarget.id;

                // create id for file input's label and upload btn
                var labelId = inputId + '-label';
                var btnId = 'btn--upload-' + inputId;
                
                // if file does not exist, return since there's nothing to do
                if(!event.currentTarget.files[0]){
                    return;
                }
                
                // set the value for the label to the label <- filename
                // label = event.currentTarget.files[0].name;
                $('#'+labelId).val(label);

                // enable/disable buttons based on label length
                var $uploadBtn = $('#' + btnId);
                if(label.length > 0){
                    $uploadBtn.prop('disabled', false);
                    $uploadBtn.removeClass('btn-disabled').addClass('btn-primary');
                } else {
                    $uploadBtn.prop('disabled', true);
                    $uploadBtn.removeClass('btn-primary').addClass('btn-disabled');
                }
            });
                
            $("#btn--upload-file--data").click(function (evt) {
                var dataFileUpload = $("#file--data").get(0);
                if(dataFileUpload.files.length > 0){
                    self._reader.readAsText(dataFileUpload.files[0]);
                    //self.uploadDataFiles(dataFileUpload.files);
                }
            });

            $("#btn--upload-file--matrix").click(function (evt) {
                var matrixFileUpload = $("#file--matrix").get(0);
                if(matrixFileUpload.files.length > 0){
                    self.uploadMatrixFiles(matrixFileUpload.files);
                }            
            });

            $(window).on('fileImport:getDataFilename', function(){
                return self.dataFilename;
            });
        },

        _initializeFileReader: function(){
            this._reader = new FileReader();
            this._reader.onload = function(evt){
                var sFile;
                try {
                    sFile = matrixHelpers.ParseS2P(evt.target.result);
                    // add real numbers to the data
                    $(window).trigger('sPlot:measurement:loaded', sFile);
                } catch(e) {
                    alert(e.message);
                }
            };
        },

        _initializeKeyEventHandler: function() {
            $(document).keyup(function (event){
                if(event.which === 68){
                    // trigger plot clicked
                    $("#btn--upload-file--data").trigger('click');
                } else if(event.which === 71){
                    // trigger goldenMatrix upload clicked
                    $("#btn--upload-file--matrix").trigger('click');
                }
            });            
        },

        uploadDataFiles: function(files){
            var self = this;
            options = {
                url: '/Matrix/UploadDataFilesAjax',
                success: function (response) {
                    var data = $.parseJSON(response);
                    self.dataFilename = data.filename;
                    $(window).trigger('measurementData:loaded', data);
                },
                error: function () {
                    alert("There was error uploading data files!");
                }
            };
            
            self.uploadFiles(files, options)
        },
        
        uploadMatrixFiles: function(files){
            var self = this;
            options = {
                url: '/Matrix/UploadMatrixFilesAjax',
                success: function (response) {
                    var data = $.parseJSON(response);
                    $(window).trigger('goldenMatrix:loaded', data);
                },
                error: function () {
                    alert("There was error uploading matrix files!");
                }
            };
            self.uploadFiles(files, options)
        },
        
        uploadFiles: function(files, options){
            var self = this;
            var data = new FormData();
            for (var i = 0; i < files.length ; i++) {
                data.append(files[i].name, files[i]);
            }
            
            var ajaxOptions = {
                type: "POST",
                contentType: false,
                processData: false,
                data: data
            };
            
            // combine options
            ajaxOptions = $.extend(true, ajaxOptions, options)
            $.ajax(ajaxOptions);
        }
    };
});