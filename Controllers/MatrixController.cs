using System;
using System.IO;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using MatrixSynthesisWebApp.Models;

namespace MatrixSynthesisWebApp.Controllers
{
    [Authorize]
    public class MatrixController : Controller
    {

        private IHostingEnvironment hostingEnv;

        public MatrixController(IHostingEnvironment env)
        {
            this.hostingEnv = env;
        }
        /*
        private ApplicationDbContext _context;
        public MatrixController(IHostingEnvironment env, ApplicationDbContext context)
        {
            this.hostingEnv = env;
            this._context = context;
        }
        */
    
        // GET: /Matrix/Index
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult UploadDataFilesAjax()
        {
            long size = 0;
            var files = Request.Form.Files;
            var filename = "";
            var content = new Tuple<string, double>("", 0);
            foreach (var file in files)
            {
                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    var fileContent = reader.ReadToEnd();
                    var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                    
                    filename = parsedContentDisposition.FileName.Trim('"');
                    size = file.Length;
                }
                
                Console.WriteLine("Time to save the file...");
                filename = hostingEnv.WebRootPath + $@"/uploads/{filename}";
                using (var fileStream = new FileStream(filename, FileMode.Create))
                {
                    var inputStream = file.OpenReadStream();
                    inputStream.CopyTo(fileStream);
                }
                Console.WriteLine("...file saved!");
                
                content = ExecuteScript("Case_A.py", filename);
            }

            // Now that the file is saved and we have the content
            // create json with S11 and S21 data only (throwing S31 away)
            JObject chartData = JObject.Parse(content.Item1);
            chartData.Add("filename", filename);
            chartData.Add("processTime", content.Item2);
            
            return Json(chartData.ToString());
        }

        private static Tuple<string, double> ExecuteScript(string scriptName, string arguments, bool isJson=false){
            // Let's start a process and get output
            var python = @"env/Scripts/python";
            var scriptArguments = "";
            if(isJson){
                arguments = arguments.Replace("\"", "\\\"");
            }
            scriptArguments = "Scripts/" + scriptName + " \"" + arguments + "\"";

            var myProcessStartInfo = new ProcessStartInfo(python);
            
            myProcessStartInfo.UseShellExecute = false;
            myProcessStartInfo.RedirectStandardOutput = true;
            myProcessStartInfo.Arguments = scriptArguments;
                            
            var myProcess = new Process();
            myProcess.StartInfo = myProcessStartInfo;
            
            Console.WriteLine("Calling Python with arguments {0}", myProcessStartInfo.Arguments);
            // capture the start time
            var startTime = DateTime.Now;
            myProcess.Start();
            
            StreamReader myStreamReader = myProcess.StandardOutput;
            var content = myStreamReader.ReadToEnd();
            
            myProcess.WaitForExit();
            var processTime = (DateTime.Now - startTime).TotalSeconds;
            //myProcess.Close();
            
            //Console.WriteLine("process time:" + processTime);
            //Console.WriteLine("Output:" + content);

            var returnContent = new Tuple<string, double>(content, processTime);

            return returnContent;
        }
        
        /*
        [HttpPost]
        public IActionResult SaveProject([FromBody]UserProject value)
        public IActionResult ExtractMatrixC([FromBody]ProjectInfo value)
        {
            // Need to get the data from request
            return Json(value);
                
        }
        */

        [HttpPost]
        public IActionResult PlotGoldenMatrix([FromBody]PlotGoldenMatrixRequest request)
        {
            var scriptToRun = "Case_A1.py";

            var requestJson = JObject.FromObject(request);
            var parameters = requestJson.ToString(0);

            var content = ExecuteScript(scriptToRun, parameters, true);

            JObject data = JObject.Parse(content.Item1);
            data.Add("processTime", content.Item2);            
            
            return Json(data.ToString(0));
        }        

        [HttpPost]
        public IActionResult ExtractMatrix([FromBody]MatrixExtractionCompactRequest request)
        {
            var requestJson = JObject.FromObject(request);
            var jsonParams = requestJson.ToString(0);


            var url = "https://synmatrix.pythonanywhere.com/ExtractMatrix?" + DateTime.Now;

            JObject content = postContent(url, jsonParams).Result;
            //JObject data = JObject.Parse(response.Content);
            //data.Add("processTime", content.Item2);
            
            return Json(content.ToString(0));
        }

        static async Task<JObject> postContent(string url, string content)
        { 
            var httpClient = new HttpClient();
            var requestData = new StringContent(
                content,
                Encoding.UTF8,
                "application/json");

            Console.WriteLine("Content:::"+content);

            try
            {
                HttpResponseMessage response = httpClient.PostAsync(url, requestData).Result;
                
                using (HttpContent responseContent = response.Content)
                {
                    string data = responseContent.ReadAsStringAsync().Result;
                    Console.WriteLine("data:::::::"+data);
                    return JObject.Parse(data);
                }
                
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Exception exSub in ex.LoaderExceptions)
                {
                    sb.AppendLine(exSub.Message);
                    sb.AppendLine();
                }
                string errorMessage = sb.ToString();
                //Display or log the error based on your application.
                Console.WriteLine("error:"+errorMessage);
            }
            return JObject.Parse("{}");
        } 

        /*  This is for python calls
        [HttpPost]
        public IActionResult ExtractMatrix([FromBody]MatrixExtractionRequest request)
        {
            // if goldenMatrix's length is zero, then it's case C
            // else, it's case E
            var caseId = "C";
            if(request.goldenMatrix.Length > 0){
                caseId = "E";
            }

            // need to determine if this is custom topology or folder
            // if customTopology's length is greater than zero, then
            //    it's going to be custom, 
            // else it's going to be folder
            var structure = "Folder";
            if(request.customStructure.Length > 0){
                structure = "Custom";
            }

            var scriptToRun = "Case_" + caseId + "_" + structure + ".py";
            Console.WriteLine("Executing {0}", scriptToRun);

            var requestJson = JObject.FromObject(request);
            var parameters = requestJson.ToString(0);

            var content = ExecuteScript(scriptToRun, parameters, true);

            JObject data = JObject.Parse(content.Item1);
            data.Add("processTime", content.Item2);
            
            return Json(data.ToString(0));
        }
        */

        [HttpPost]
        public IActionResult UploadMatrixFilesAjax()
        {
            long size = 0;
            var files = Request.Form.Files;
            var filename = "";
            var content = "";
            foreach (var file in files)
            {
                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    var fileContent = reader.ReadToEnd();
                    var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                    
                    filename = parsedContentDisposition.FileName.Trim('"');
                    content = fileContent;
                    size = file.Length;
                }
                
                filename = hostingEnv.WebRootPath + $@"/uploads/{filename}";
                using (var fileStream = new FileStream(filename, FileMode.Create))
                {
                    var inputStream = file.OpenReadStream();
                    inputStream.CopyTo(fileStream);
                }
                //file.SaveAs(filename);
            }

            // Now that the file is saved and we have the content
            // convert into a json object in the form of an array of arrays

            // Step 1, convert '  ' into ',' and ' -' into ',-'
            
            // TODO - use stringbuilder
            var jsonItems = content.Trim() ; //.Replace("  ", ",").Replace(" -", ",-");
            string aLine, json = "{\"goldenMatrix\":[";
            var strReader = new StringReader(jsonItems);
            var i = 0;
            while(true)
            {
                aLine = strReader.ReadLine();
                if (aLine != null) {
                    // trim line
                    aLine = aLine.Trim().Replace("  ", ",").Replace(" -", ",-").Replace(" ", ",");
                    // if this is not the first row, add a comma
                    if(aLine.Replace(" ", string.Empty).Length > 0){
                        if(i++ > 0){
                            json += ",";
                        }
                        json += "[" + aLine + "]";
                    }
                } else {
                    break;
                }
            }
            json += "]}";
            
            Console.WriteLine(json);
            return Json(json);
        }        
        
    }
}